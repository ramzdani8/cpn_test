/*
 Navicat Premium Data Transfer

 Source Server         : MyPostgre
 Source Server Type    : PostgreSQL
 Source Server Version : 100011
 Source Host           : localhost:5432
 Source Catalog        : chn_test
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 100011
 File Encoding         : 65001

 Date: 25/09/2020 14:26:15
*/


-- ----------------------------
-- Sequence structure for cuti_karyawan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."cuti_karyawan_seq";
CREATE SEQUENCE "public"."cuti_karyawan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Table structure for cuti_karyawan
-- ----------------------------
DROP TABLE IF EXISTS "public"."cuti_karyawan";
CREATE TABLE "public"."cuti_karyawan" (
  "id" int4 NOT NULL DEFAULT nextval('cuti_karyawan_seq'::regclass),
  "nomor_induk" varchar(10) COLLATE "pg_catalog"."default",
  "tanggal_mulai" date,
  "lama_cuti" int2,
  "keterangan" text COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of cuti_karyawan
-- ----------------------------
INSERT INTO "public"."cuti_karyawan" VALUES (1, 'IP06001', '2019-02-01', 3, 'Acara keluar');
INSERT INTO "public"."cuti_karyawan" VALUES (2, 'IP06001', '2019-02-13', 2, 'Anak sakit');
INSERT INTO "public"."cuti_karyawan" VALUES (3, 'IP07007', '2019-02-15', 1, 'Nenek sakit');
INSERT INTO "public"."cuti_karyawan" VALUES (4, 'IP06003', '2019-02-17', 1, 'Mendaftar sekolah anak');
INSERT INTO "public"."cuti_karyawan" VALUES (5, 'IP07006', '2019-08-20', 3, 'Menikah');
INSERT INTO "public"."cuti_karyawan" VALUES (6, 'IP07004', '2019-08-13', 3, 'Imunisasi anak');

-- ----------------------------
-- Table structure for karyawan
-- ----------------------------
DROP TABLE IF EXISTS "public"."karyawan";
CREATE TABLE "public"."karyawan" (
  "nomor_induk" varchar(10) COLLATE "pg_catalog"."default" NOT NULL,
  "nama" varchar(30) COLLATE "pg_catalog"."default",
  "alamat" text COLLATE "pg_catalog"."default",
  "tanggal_lahir" date,
  "tanggal_masuk" date
)
;

-- ----------------------------
-- Records of karyawan
-- ----------------------------
INSERT INTO "public"."karyawan" VALUES ('IP06001', 'Agus', 'Jln. Gajah Mada 115A Jakarta Pusat', '1970-08-01', '2006-07-07');
INSERT INTO "public"."karyawan" VALUES ('IP06002', 'Amin', 'Jln. Bungur sari v No, 178, Bandung', '1977-05-03', '2006-07-07');
INSERT INTO "public"."karyawan" VALUES ('IP06003', 'Yusuf', 'Jln. Yosodpuro 15, Surabaya', '1973-08-09', '2006-07-07');
INSERT INTO "public"."karyawan" VALUES ('IP07004', 'Alyssa', 'Jln. Cendana No.6 Bandung', '1983-02-14', '2007-01-05');
INSERT INTO "public"."karyawan" VALUES ('IP07005', 'Maulana', 'Jln. Ampera Raya No 1', '1985-10-10', '2007-02-05');
INSERT INTO "public"."karyawan" VALUES ('IP07006', 'Afika', 'Jln. Pejaten Barat No 6A', '1987-03-09', '2007-06-09');
INSERT INTO "public"."karyawan" VALUES ('IP07007', 'James', 'Jln. Padjadjaran No.111 Bandung', '1988-05-19', '2007-06-09');
INSERT INTO "public"."karyawan" VALUES ('IP09008', 'Octavanus', 'Jln. Gajah Mada 101. Semarang', '1988-10-07', '2008-08-08');
INSERT INTO "public"."karyawan" VALUES ('IP09009', 'Nugroho', 'Jln. Duren Tiga 196, Jakarta Selatan', '1988-01-20', '2008-11-11');
INSERT INTO "public"."karyawan" VALUES ('IP090010', 'Raisa', 'Jln. Nangka Jakarta Selatan', '1989-12-29', '2009-02-09');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS "public"."user";
CREATE TABLE "public"."user" (
  "id" int4 NOT NULL,
  "email" varchar(50) COLLATE "pg_catalog"."default",
  "password" varchar(60) COLLATE "pg_catalog"."default",
  "nama" varchar(70) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO "public"."user" VALUES (1, 'admin@gmail.com', '$2y$10$tonZkQrnGnp9n38rWeMTieLPNxtDfvy4Z/35Q4rlFObsm/xFnSae.', 'Admin');

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."cuti_karyawan_seq"
OWNED BY "public"."cuti_karyawan"."id";
SELECT setval('"public"."cuti_karyawan_seq"', 7, true);

-- ----------------------------
-- Primary Key structure for table cuti_karyawan
-- ----------------------------
ALTER TABLE "public"."cuti_karyawan" ADD CONSTRAINT "cuti_karyawan_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table karyawan
-- ----------------------------
ALTER TABLE "public"."karyawan" ADD CONSTRAINT "karyawan_pkey" PRIMARY KEY ("nomor_induk");

-- ----------------------------
-- Primary Key structure for table user
-- ----------------------------
ALTER TABLE "public"."user" ADD CONSTRAINT "user_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Keys structure for table cuti_karyawan
-- ----------------------------
ALTER TABLE "public"."cuti_karyawan" ADD CONSTRAINT "cuti_karyawan_nomor_induk_fkey" FOREIGN KEY ("nomor_induk") REFERENCES "public"."karyawan" ("nomor_induk") ON DELETE NO ACTION ON UPDATE NO ACTION;
