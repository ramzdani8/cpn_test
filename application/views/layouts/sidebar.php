<body>
<div id="app">
	<div class="main-wrapper main-wrapper-1">
		<div class="navbar-bg"></div>
		<nav class="navbar navbar-expand-lg main-navbar">
			<form class="form-inline mr-auto">
				<ul class="navbar-nav mr-3">
					<li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a>
					</li>
				</ul>
			</form>
			<ul class="navbar-nav navbar-right">
				<li class="dropdown"><a href="#" data-toggle="dropdown"
										class="nav-link dropdown-toggle nav-link-lg nav-link-user">
						<img alt="image" src="assets/img/avatar/avatar-1.png" class="rounded-circle mr-1">
						<div class="d-sm-none d-lg-inline-block">Hi, Admin</div>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
						<div class="dropdown-divider"></div>
						<a href="<?= base_url(); ?>login/logout" class="dropdown-item has-icon text-danger">
							<i class="fas fa-sign-out-alt"></i> Logout
						</a>
					</div>
				</li>
			</ul>
		</nav>
		<div class="main-sidebar sidebar-style-2">
			<aside id="sidebar-wrapper">
				<div class="sidebar-brand">
					<a href="<?= base_url() ?>">CPN</a>
				</div>
				<div class="sidebar-brand sidebar-brand-sm">
					<a href="<?= base_url() ?>">CPN</a>
				</div>
				<ul class="sidebar-menu">
					<li class="menu-header">Dashboard</li>
					<li class="dropdown active">
						<a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Dashboard</span></a>
						<ul class="dropdown-menu">
							<li class="active"><a class="nav-link" href="<?= base_url() ?>home">General Dashboard</a></li>
							<li><a class="nav-link" href="<?= base_url() ?>karyawan">Data Karyawan</a></li>
							<li><a class="nav-link" href="<?= base_url() ?>cutikaryawan">Data Cuti</a></li>
						</ul>
					</li>
					<li class="menu-header">Other</li>
					<li class="dropdown">
						<a href="#" class="nav-link has-dropdown"><i class="fas fa-th-large"></i><span>Soal</span></a>
						<ul class="dropdown-menu">
							<li><a class="nav-link" href="<?= base_url() ?>anagram">1. Anagram</a></li>
							<li><a class="nav-link" href="<?= base_url() ?>kelipatan">2. Kelipatan Bilangan</a></li>
							<li><a class="nav-link" href="<?= base_url() ?>query">3. SQL Query</a></li>
						</ul>
					</li>
				</ul>

				<div class="mt-4 mb-4 p-3 hide-sidebar-mini">
					<a href="https://getstisla.com/docs" class="btn btn-primary btn-lg btn-block btn-icon-split">
						<i class="fas fa-rocket"></i> Documentation
					</a>
				</div>
			</aside>
		</div>
