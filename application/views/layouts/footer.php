
<footer class="main-footer">
	<div class="footer-right">

	</div>
</footer>
</div>
</div>

<!-- General JS Scripts -->
<script src="<?= base_url(); ?>/assets/modules/jquery.min.js"></script>
<script src="<?= base_url(); ?>/assets/modules/popper.js"></script>
<script src="<?= base_url(); ?>/assets/modules/tooltip.js"></script>
<script src="<?= base_url(); ?>/assets/modules/bootstrap/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>/assets/modules/nicescroll/jquery.nicescroll.min.js"></script>
<script src="<?= base_url(); ?>/assets/modules/moment.min.js"></script>
<script src="<?= base_url(); ?>/assets/js/stisla.js"></script>

<!-- JS Libraies -->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url(); ?>/assets/modules/jquery.sparkline.min.js"></script>
<script src="<?= base_url(); ?>/assets/modules/chart.min.js"></script>
<script src="<?= base_url(); ?>/assets/modules/owlcarousel2/dist/owl.carousel.min.js"></script>
<script src="<?= base_url(); ?>/assets/modules/summernote/summernote-bs4.js"></script>
<script src="<?= base_url(); ?>/assets/modules/chocolat/dist/js/jquery.chocolat.min.js"></script>

<!-- Page Specific JS File -->
<script src="<?= base_url(); ?>/assets/js/page/index.js"></script>
<script src="<?= base_url(); ?>/assets/modules/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
		{
			return {
				"iStart": oSettings._iDisplayStart,
				"iEnd": oSettings.fnDisplayEnd(),
				"iLength": oSettings._iDisplayLength,
				"iTotal": oSettings.fnRecordsTotal(),
				"iFilteredTotal": oSettings.fnRecordsDisplay(),
				"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
				"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
			};
		};
		var t = $("#myTableKaryawan").dataTable({
			initComplete: function() {
				var api = this.api();
				$('#mytable_filter input')
						.off('.DT')
						.on('keyup.DT', function(e) {
							if (e.keyCode == 13) {
								api.search(this.value).draw();
							}
						});
			},
			oLanguage: {
				sProcessing: "loading..."
			},
			processing: true,
			serverSide: true,
			ajax: {"url": "karyawan/json", "type": "POST"},
			columns: [
				{
					"data": "nomor_induk",
					"orderable": false
				},
				{"data": "nama"},
				{"data": "alamat"},
				{"data": "tanggal_lahir"},
				{"data": "tanggal_masuk"},
				{"data": "view"}
			],
			order: [[1, 'asc']],
			rowCallback: function(row, data, iDisplayIndex) {
				var info = this.fnPagingInfo();
				var page = info.iPage;
				var length = info.iLength;
				var index = page * length + (iDisplayIndex + 1);
				$('td:eq(0)', row).html(index);
			}
		});

		var d = $("#myTableCutiKaryawan").dataTable({
			initComplete: function() {
				var api = this.api();
				$('#mytable_filter input')
						.off('.DT')
						.on('keyup.DT', function(e) {
							if (e.keyCode == 13) {
								api.search(this.value).draw();
							}
						});
			},
			oLanguage: {
				sProcessing: "loading..."
			},
			processing: true,
			serverSide: true,
			ajax: {"url": "cutikaryawan/json", "type": "POST"},
			columns: [
				{
					"data": "nomor_induk",
					"orderable": false
				},
				{"data": "nama"},
				{"data": "tanggal_mulai"},
				{"data": "lama_cuti"},
				{"data": "keterangan"},
				{"data": "view"}
			],
			order: [[1, 'asc']],
			rowCallback: function(row, data, iDisplayIndex) {
				var info = this.fnPagingInfo();
				var page = info.iPage;
				var length = info.iLength;
				var index = page * length + (iDisplayIndex + 1);
				$('td:eq(0)', row).html(index);
			}
		});
	});
</script>

<!-- Template JS File -->
<script src="<?= base_url(); ?>/assets/js/scripts.js"></script>
<script src="<?= base_url(); ?>/assets/js/custom.js"></script>
</body>
</html>
</html>
