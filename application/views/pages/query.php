<div class="main-content">
	<section class="section">
		<div class="section-header">
			<h1>SQL Query Result</h1>
		</div>

		<div class="section-body">
			<h2 class="section-title">No.3 SQL Query </h2>
			<p class="section-lead">
			</p>

			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header">
							<h4>DB : Postgre</h4>
						</div>
						<div class="card-body">
							<div class="list-group">
								<a href="#" class="list-group-item list-group-item-action flex-column align-items-start active">
									<div class="d-flex w-100 justify-content-between">
										<h5 class="mb-1">Menampilkan 3 karyawan yang pertama kali masuk.</h5>
										<small>No.1 </small>
									</div>
									<p class="mb-1">
										SELECT
										"public".karyawan.nomor_induk,
										"public".karyawan.nama,
										"public".karyawan.alamat,
										"public".karyawan.tanggal_lahir,
										"public".karyawan.tanggal_masuk
										FROM
										"public".karyawan
										ORDER BY
										"public".karyawan.tanggal_masuk ASC
										LIMIT 3
									</p>
								</a>
								<a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
									<div class="d-flex w-100 justify-content-between">
										<h5 class="mb-1">Menampilkan daftar karyawan yang saat ini sedang cuti. Daftar berisi <b>nomor_induk</b>,
										<b>nama</b>, <b>tanggal_mulai</b>, <b>lama_cuti</b>, dan keterangan.</h5>
										<small class="text-muted">No.2</small>
									</div>
									<p class="mb-1">
										SELECT
										"public".cuti_karyawan.nomor_induk,
										"public".karyawan.nama,
										"public".cuti_karyawan.tanggal_mulai,
										"public".cuti_karyawan.lama_cuti,
										"public".cuti_karyawan.keterangan
										FROM
										"public".karyawan
										INNER JOIN "public".cuti_karyawan ON "public".cuti_karyawan.nomor_induk = "public".karyawan.nomor_induk
									</p>
								</a>
								<a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
									<div class="d-flex w-100 justify-content-between">
										<h5 class="mb-1">Menampilkan daftar karyawan yang sudah cuti lebih dari satu kali. Daftar berisi <b>nomor_induk</b>,
											<b>nama</b>, <b>jumlah</b>.</h5>
										<small class="text-muted">No.3</small>
									</div>
									<p class="mb-1">
										SELECT
										"public".cuti_karyawan.nomor_induk,
										"public".karyawan.nama,
										COUNT ("public".cuti_karyawan.nomor_induk) AS jumlah
										FROM
										"public".karyawan
										INNER JOIN "public".cuti_karyawan ON "public".cuti_karyawan.nomor_induk = "public".karyawan.nomor_induk
										GROUP BY
										"public".cuti_karyawan.nomor_induk,
										"public".karyawan.nama
										HAVING
										COUNT ("public".cuti_karyawan.nomor_induk) > 1
									</p>
								</a>
								<a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
									<div class="d-flex w-100 justify-content-between">
										<h5 class="mb-1">Menampilkan sisa cuti tiap karyawan tahun ini, Diketahui jatah cuti
											tiap tahun ini adalah 12. Daftar berisi <b>nomor_induk</b>,
											<b>nama</b>, <b>nama</b>, <b>sisa_cuti</b>.</h5>
										<small class="text-muted">No.4</small>
									</div>
									<p class="mb-1">
										SELECT
										"public".cuti_karyawan.nomor_induk,
										"public".karyawan.nama,
										(12 - COUNT ("public".cuti_karyawan.nomor_induk)) AS sisa_cuti
										FROM
										"public".karyawan
										INNER JOIN "public".cuti_karyawan ON "public".cuti_karyawan.nomor_induk = "public".karyawan.nomor_induk
										GROUP BY
										"public".cuti_karyawan.nomor_induk,
										"public".karyawan.nama
									</p>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
