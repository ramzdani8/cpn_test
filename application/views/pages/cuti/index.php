<div class="main-content">
	<section class="section">
		<div class="section-header">
			<h1>Cuti Karyawan</h1>
			<div class="section-header-breadcrumb">
				<div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
				<div class="breadcrumb-item"><a href="#">Cuti Karyawan</a></div>
				<div class="breadcrumb-item">Data</div>
			</div>
		</div>

		<div class="section-body">
			<h2 class="section-title">Table</h2>

			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header">
							<h4>Cuti Karyawan Table</h4>
						</div>
						<div class="card-body p-0">
							<div class="table-responsive">
								<table class="table table-striped" id="myTableCutiKaryawan">
									<thead>
									<tr>
										<th>Nomor Induk</th>
										<th>Nama</th>
										<th>Tanggal Mulai</th>
										<th>Durasi</th>
										<th>Keterangan</th>
										<th>Action</th>
									</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</section>
</div>
