<div class="main-content">
	<section class="section">
		<div class="section-header">
			<h1>Check Anagram</h1>
		</div>

		<div class="section-body">
			<h2 class="section-title">Check Anagram</h2>

			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header">
							<h4>Input Text</h4>
						</div>
						<div class="card-body">
							<form action="<?= base_url() ?>anagram/proses" method="POST">
								<div class="row">
									<div class="form-group col-6">
										<label>Kata Ke-1</label>
										<div class="input-group">
											<input name="str1" type="text" class="form-control">
										</div>
									</div>
									<div class="form-group col-6">
										<label>Kata Ke-2</label>
										<div class="input-group">
											<input name="str2" type="text" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<div class="input-group">
											<button type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
