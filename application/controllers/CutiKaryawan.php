<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CutiKaryawan extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//Load Model
		$this->load->library('datatables');
		$this->load->model('CutiKaryawanModel');
		$this->load->model('UserModel');
		if($this->UserModel->isNotLogin()) redirect(site_url('login'));
	}

	public function index(){
		$data = array(
			'judul' => 'Karyawan'
		);
		$this->load->view('layouts/header',$data);
		$this->load->view('layouts/sidebar');
		$this->load->view('pages/cuti/index');
		$this->load->view('layouts/footer');
	}

	function json() {
		header('Content-Type: application/json');
		echo $this->CutiKaryawanModel->json();
	}
}
