<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Query extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('UserModel');
		if($this->UserModel->isNotLogin()) redirect(site_url('login'));
	}

	public function index()
	{
		$this->load->view('layouts/header');
		$this->load->view('layouts/sidebar');
		$this->load->view('pages/query');
		$this->load->view('layouts/footer');
	}
}
