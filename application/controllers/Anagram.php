<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anagram extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('UserModel');
		if($this->UserModel->isNotLogin()) redirect(site_url('login'));
	}

	public function index()
	{
		$this->load->view('layouts/header');
		$this->load->view('layouts/sidebar');
		$this->load->view('pages/anagram/index');
		$this->load->view('layouts/footer');
	}

	public function proses()
	{
		$str1 = $this->input->post('str1');
		$str2 = $this->input->post('str2');

		$check = $this->isAnagram($str1, $str2);
		$hasil = "Hasil Dari Kata Ke-1 <b>".$str1."</b> Dan Kata Ke-2 <b>".$str2."</b> Adalah ".($check ? "Anagram" : "Bukan Anagram");

		$data = array(
			'hasil' => $hasil
		);

		$this->load->view('layouts/header');
		$this->load->view('layouts/sidebar');
		$this->load->view('pages/anagram/result',$data);
		$this->load->view('layouts/footer');

	}

	function isAnagram($str1, $str2)
	{
		// check if both string have the same length.
		if (strlen($str1) !== strlen($str2)) {
			return false;
		}

		$t_s1 = Array();
		$t_s2 = Array();

		$len = strlen($str1);

		for ($i=0;$i<$len;$i++) {

			// map str1 character count
			if (empty($t_s1[$str1[$i]])) {
				$t_s1[$str1[$i]] = 1;
			}
			else {
				$t_s1[$str1[$i]]++;
			}

			// map str2 character count
			if (empty($t_s2[$str2[$i]])) {
				$t_s2[$str2[$i]] = 1;
			}
			else {
				$t_s2[$str2[$i]]++;
			}
		}

		// debug
		//print_r($t_s1);
		//print_r($t_s2);

		// if the character count is not same, that's mean the string is not anagram
		foreach ($t_s1 as $k => &$v) {
			if ($t_s1[$k] !== $t_s2[$k]) {
				return false;
			}
		}

		return true;
	}

	public function is_anagram($a, $b) {
		return(count_chars($a, 1) == count_chars($b, 1));
	}
}
