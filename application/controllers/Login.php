<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("UserModel");
		//$this->load->library('form_validation');
	}

	public function index()
	{
		$this->load->view('v_login');
	}

	public function auth(){
		// jika form login disubmit
		if($this->input->post()){
			if($this->UserModel->doLogin()) redirect(site_url('home'));
		}
		// tampilkan halaman login
		$this->load->view("v_login");
	}

	public function logout()
	{
		// hancurkan semua sesi
		$this->session->sess_destroy();
		redirect(site_url('login'));
	}

}
