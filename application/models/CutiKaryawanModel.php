<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CutiKaryawanModel extends CI_Model{
	public function __construct()
	{
		parent::__construct();
	}

	public function json(){

		$this->datatables->select('
			cuti_karyawan.nomor_induk,
			karyawan.nama,
			cuti_karyawan.tanggal_mulai,
			cuti_karyawan.lama_cuti,
			cuti_karyawan.keterangan
		');
		$this->datatables->from('cuti_karyawan');
		$this->datatables->join('karyawan', 'karyawan.nomor_induk = cuti_karyawan.nomor_induk');
		$this->datatables->add_column('view', '<a href="cutikaryawan/edit/$1">edit</a> | <a href="cutikaryawan/delete/$1">delete</a>', 'nomor_induk');
		return $this->datatables->generate();
	}
}
