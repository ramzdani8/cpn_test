<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KaryawanModel extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function json(){
		$this->datatables->select('nomor_induk,nama,alamat,tanggal_lahir,tanggal_masuk');
		$this->datatables->from('karyawan');
		$this->datatables->add_column('view', '<a href="karyawan/edit/$1">edit</a> | <a href="karyawan/delete/$1">delete</a>', 'nomor_induk');
		return $this->datatables->generate();
	}
}
